<?php

namespace Tests\Feature;

use App\User;
use App\Event;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EventTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testAddEvent(){
        $this->organiserUser = User::create([
            'firstName' => "Organiser",
            'lastName' => "User",
            'dob' => Carbon::now(),
            'username' => "organiser",
            'email' => "organiser@organiser.com",
            'password' => Hash::make("organiser"),
            'role' => 2,
        ]);

        $response = $this->actingAs($this->organiserUser)->post('/events', [
            'name' => 'Event 1',
            'description' => "This is a description.. Event of the year",
            'time' => Carbon::now()->addMonth(),
            'venue' => "Stonehedge",
            'speakerSponsors' => null,
            // 'organsiers' => $this->organiserUser->id,
            'notes' => "Reminder this is a test",
        ]);

        $event = Event::first();
        $response->assertStatus(302);
        $response->assertRedirect('/events/'.$event->id);
    }

    // public function testAddEventNoDate(){

    // }

    // public function testAddEventNoVenue(){
        
    // }
}
