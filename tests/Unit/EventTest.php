<?php

namespace Tests\Unit;

use App\User;
use App\Event;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventTest extends TestCase
{
    use RefreshDatabase;

    protected $adminUser, $organiserUser;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testAddEvent(){
        $this->organiserUser = User::create([
            'firstName' => "Organiser",
            'lastName' => "User",
            'dob' => Carbon::now(),
            'username' => "organiser",
            'email' => "organiser@organiser.com",
            'password' => Hash::make("organiser"),
            'role' => 2,
        ]);

        $response = $this->actingAs($this->organiserUser)->post('/events', [
            'name' => 'Event 1',
            'description' => "This is a description.. Event of the year",
            'time' => Carbon::now()->addMonth(),
            'venue' => null,
            'speakerSponsors' => null,
            'organsiers' => $this->organiserUser->id,
            'notes' => "Reminder this is a test",
        ]);

        $this->assertCount(1, Event::all());
        $this->assertDatabaseHas('events', [
            'name' => "Event 1",
            'organisers' => $this->organiserUser->id
        ]);
    }
}
