<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        // factory(App\Event::class, 19)->create()->each(function($event){
        //     $event->speakers()->save(factory(App\User::class)->make());
        // });
    }
}
