<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();               // User/Speaker/Sponsor ID [Primary ID]
            $table->string('firstName');            // User First Name
            $table->string('lastName');             // User Last Name
            $table->date('dob');                    // User DOB
            $table->string('username')->unique();   // UserName (Testing Login)
            $table->string('email')->unique();      // Email Address (Login Details)
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');             // Password
            $table->enum('role', [0,1,2,3])         // User Role
            /**
             * None = Guest User (SHOULD NOT BE ALLOWED AS NULL)
             * 0 = Subscribed user
             * 1 = Admin [Only Admin can initialize 2 and 3]
             * 2 = Sponsor
             * 3 = Speaker
             * */
            ->default('0');
            $table->string('avatarPic')->default('default.png');// User Avatar Pic
            $table->string('userWebsite')           // User Website
                    ->nullable();
            $table->rememberToken();                // Token
            $table->timestamps();                   // Create/Update Timestamps
            $table->softDeletes();                  // Soft Deleting Timestamp
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
