<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();                           // Event ID (Cannot be randomized) [Primary Key]
            $table->string('name')->comment("Event Name");                             // Event Name
            $table->string('description', 1000)->comment("Event Description");                // Event Description
            $table->dateTime('time')->comment("Event Time");                           // Date of the Event
            $table->string('venue')->comment("Event Venue Location")->nullable();                   // Event Location
            $table->boolean('isCancelled')->default(0)->comment("Boolean: Cancelled");                           // Event Cancelled Boolean
            $table->unsignedInteger('speakSponID')->comment("Sponsors/Speakers FOREIGN KEY")->nullable();        // Sponsor/Speaker Related (User)
            $table->unsignedInteger('organisers')->comment("Organisers FOREIGN KEY");       //Organiser Related (User)
            $table->string('notes', 1000)->comment("Event Notes");                      // Event Notes
            $table->timestamps();                               // Internal Date Creation/Update
            $table->softDeletes();                  // Soft Deleting Timestamp
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
