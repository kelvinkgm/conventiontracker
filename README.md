# conventionTracker
#### Powered By [Laravel](https://github.com/laravel/laravel/blob/master/readme.md)

## Description
This is an Event Tracker for any super event, with miniture events, such as:
* Hackathons
* Tech Conventions
##### Potentional Future Uses
* Major Music Festivals
* Any Major Conventions
##### Potential Future Features
* In-building/In-event Navigation

## To Use (Use Terminal or GitBash)
1. Clone this Repo
2. Install [Composer](https://getcomposer.org)
    1. Make sure to do `composer update` to get all the required vendor packages
    2. Do a `php artisan key:generate` to generate the app key **after** creating a local .env file
3. Type `php artisan migrate` have all the databases required for this web app
4. Head to PHPMyAdmin or Sequal Pro
    * Make sure that you have XAMP or MAMP **WITH _Apache and MySQL/MariaDB ACTIVE_** 

## Licences
This is an open-source project, and not profitable. Any Liability will be dealt by the user/company/organisation using this, and cannot blame the original creator for any liabilities.