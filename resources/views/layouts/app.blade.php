<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(! Request::is('/')) @yield('title') - {{ config('app.name', 'Convention Tracker') }} @else {{ config('app.name', 'Convention Tracker') }} @endif</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel p-3">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('events.index') }}">Show All Events</a>
                        </li>
                    @else
                        @if(Auth::user()->role === '2' || Auth::user()->role === '3')
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('events.index') }}">
                                    <img src="{{asset('/icons/mytick.png')}}" alt="My Organized Events" width="35" height="35">
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('events.index') }}">Show All Events</a>
                            </li>
                        @elseif(Auth::user()->role === '0')
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('events.index') }}">
                                    <!-- <img src="{{ asset('/icons/luvtick.png') }}" alt="My Liked Events" width="35" height="35"> -->
                                    <i class="fas fa-heart"></i>
                                Loved Events</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('events.index') }}">
                                    <i class="far fa-calendar-check"></i>
                                Registered Events</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('events.index') }}">All {{config('app.name')}} Events</a>
                            </li>
                        @endif
                    @endguest
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <img src="{{asset('/storage/images/users/'.Auth::user()->avatarPic)}}" class="rounded-circle d-none d-md-inline" height="30" width="30">
                                     {{ Auth::user()->username }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('user.edit', Auth::user()) }}">Account Settings</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-1">
            @yield('content')
        </main>

        <footer class="footer container">
            <div class="row">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">

                </div>
            </div>
            <div class="row text-center">
                <div class="col-12">
                    <hr>
                    <p>&copy;{{Carbon\Carbon::now()->year}} &nbsp; {{config('app.name') }} All Rights Reserved.</p>
                </div>
            </div>
        </footer>
    </div>
</body>
</html>