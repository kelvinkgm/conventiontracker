@extends('layouts.app')

@section('content')
<!-- Jumbotron -->
<div class="jumbotron py-4">
    <div id="randomEvents" class="carousel slide" data-ride="carousel">
        @include('jumbotron')
    </div>
</div>
<div class="container">
    <!-- Upcoming Events -->
    <h3 class="text-center">Upcoming Events</h3>
    <div class="row">
        <div class="col-sm-12 col-lg-6">
            <div class="border"></div>
        </div>
        <div class="col-sm-12 col-lg-6">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td><h3 class="text-center">No More Events... Hope you had a good time at {{config('app.name', 'Convention Tracker')}}</h3></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection