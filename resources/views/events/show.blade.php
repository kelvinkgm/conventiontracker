@extends('layouts.app')
@section('title')
{{$event->name}}
@endsection
@section('content')
<div class="container mx-auto px-4 w-full">
    <h1 class="text-center">{{$event->id}}</h1>
    <p class="text-center">{{is_null($event->time) ? "TBA" : $event->time}} - {{is_null($event->venue) ? "TBA" : $event->venue}}</p>
    <hr>
    <p class="pl-3">Organisers: <a href="mailto:{{$event->email}}">{{$event->organiser->firstName.' '.$event->organiser->lastName}}</a></p>
    <div class="row">
        <div class="col-md-6">
            <h6>Description</h6>
            <p>{{$event->description}}</p>
        </div>
        <div class="col-md-6">
            <h6>Notes</h6>
            <p>{{$event->notes}}</p>
        </div>
    </div>
    @if()
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <a href="{{route('events.edit', $topic)}}" class="btn btn-warning btn-block mb-2">Edit This Event</a>
        </div>
        <div class="col-md-6 col-sm-12">
            <a href="{{route('events.show', $event)}}" onclick="alert('In Progress')" class="btn btn-warning btn-block mb-2">Cancel This Topic</a>
        </div>
    </div>
</div>
@endsection