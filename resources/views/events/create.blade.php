@extends('layouts.app')
@section('title')
Create New Event
@endsection
@section('content')
<div class="container mx-auto px-4 w-full">
    <form action="{{route('events.store')}}" method="post">
        <h2>Add new Event</h2>
        <hr>
        @csrf
        <fieldset>
            <div class="form-group">
                <label for="name">Event Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" aria-describedby="nameHelp" placeholder="Event Name" value="{{old('name')}}">
            </div>
        </fieldset>
        <div class="form-group">
            <label for="description">Event Description</label>
            <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" rows="3" placeholder="Event Description">{{ old('description') }}</textarea>
        </div>
        <div class="form-group">
            <label for="time">{{ __('Event Date') }}</label>
            <div class="col-md-6">
                <input id="time" type="date" class="form-control @error('time') is-invalid @enderror" name="time" value="{{ old('time') }}" autofocus>
                <small id="timeHelp" class="form-text text-muted">Please utilize this format for non-supported browsers. ('YYYY-MM-DD') </small>
                @if ($errors->has('time'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('time') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
                <label for="venue">Event Venue</label>
                <input type="text" class="form-control @error('venue') is-invalid @enderror" id="venue" name="venue" aria-describedby="venueHelp" placeholder="Event Venue" value="{{old('venue')}}">
            </div>
        <div class="form-group">
            <label for="description">Event Notes</label>
            <textarea class="form-control @error('notes') is-invalid @enderror" id="notes" name="notes" rows="3" placeholder="Event notes">{{ old('notes') }}</textarea>
        </div>
        <hr>
        <div class="row">
            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
            <div class="col-md-6 col-sm-12 py-1">
                <input type="submit" value="Create Event" class="btn btn-success btn-block">
            </div>
            <div class="col-md-6 col-sm-12 py-1">
                <input type="reset" value="Reset" class="btn btn-danger btn-block">
            </div>
        </div>
    </form>
</div>
@endsection