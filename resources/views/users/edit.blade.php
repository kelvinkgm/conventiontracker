@extends('layouts.app')
@section('title')
Account Settings
@endsection
@section('content')
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h2 class="text-center">Account Settings</h2>
    <hr>
    <form action="" method="post" enctype="multipart/form-data" id="eventForm">
        @csrf
        {{method_field('PATCH')}}
        <fieldset>
            <legend>Profile Picture</legend>
            <div class="row">
                <div class="col-sm-2">
                    <img src="{{asset('/storage/images/users/'.Auth::user()->avatarPic)}}" class="rounded-circle img-fluid mx-auto d-block">
                </div>
                <div class="col-sm-10">
                    <div class="form-group">
                        <label for="avatarPic">Upload Profile Picture</label>
                        <input type="file" class="form-control-file" name="avatarPic" id="avatarPic">
                    </div>
                    <!-- <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="checked" id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">Set to default?</label>
                        <small id="dobHelp" class="form-text text-muted">Please select if you wish to reset it to the default image. </small>
                    </div> -->
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Public Information</legend>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="firstName">First Name: </label>
                        <input type="text" class="form-control" name="firstName" placeholder="First Name" value="{{$account->firstName}}" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastName">Surname: </label>
                        <input type="text" class="form-control" name="lastName" placeholder="Surname" value="{{$account->lastName}}" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="username">Username: </label>
                        <input type="text" class="form-control" name="username" placeholder="Username" value="{{$account->username}}" required>
                        <small id="usernameHelp" class="form-text text-muted">This will digitally identify you during the convention.</small>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="dob">Birthday</label>
                        <input type="date" name="dob" placeholder="2000-01-01" class="form-control" value="{{$account->dob}}"required>
                        <small id="dobHelp" class="form-text text-muted">Please utilize this format for non-supported browsers. ('YYYY-MM-DD') </small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="email">Registered Email Address</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{$account->email}}" disabled>
                    </div>
                </div>
            </div>
        </fieldset>
        
        <hr>
        <fieldset>            
            <legend>Change Password</legend>
            <div class="form-group">
                <label for="current-password">Current Password: </label>
                <input type="password" class="form-control" name="current-password" placeholder="Current Password">
            </div>
            <div class="form-group">
                <label for="password">New Password: </label>
                <input type="password" class="form-control" name="password" placeholder="New Password">
            </div>
            <div class="form-group">
                <label for="confirm-password">Re-enter New Password: </label>
                <input type="password" class="form-control" name="confirm-password" placeholder="Confirm Password">
            </div>
        </fieldset>
        <hr>
        <fieldset>
            <div class="row form-group">
                <input type="hidden" name="submitted" value="{{ csrf_token() }}">
                <div class="col-lg-6">
                    <input type="submit" class="btn btn-success form-control" name="submit" value="Update Account">
                    {{ method_field('PUT') }}
                </div>
                <div class="col-lg-6">
                    <input type="reset" class="btn btn-primary form-control" name="reset">
                </div>
            </div>
        </fieldset>
    </form>
</div>
@endsection