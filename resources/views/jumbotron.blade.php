<div class="carousel-inner">
    @forelse($randomEvents as $event)
        @if($loop->first)
            <div class="carousel-item active">
        @else
            <div class="carousel-item">
        @endif
            <img class="img-fluid mx-auto d-block" src="{{asset('/storage/images/event/'.$event->imageName)}}" alt="{{$event->name}}">
            <div class="carousel-caption">
                <h5>{{$event->name}}</h5>
                <p class="d-none d-md-block">{{$event->description}}</p>
            </div>
    </div>
    @empty
        <h3 class="text-center">No More Events... Hope you had a good time at {{config('app.name', 'Convention Tracker')}}</h3>
    @endforelse
</div>