<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{

    public function __construct(){
        $this->middleware('auth')->except('index');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->role != 0){
            
        } else {
            return abort(404, "Not Found");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->role != 0){
            $validatedData = $request->validate(
                // $request, Event::validationMessages() ,Event::validationRules()
                [
                    'name' => 'required|string',
                    'description' => 'required|string',
                    'time' => 'sometimes|nullable|date',
                    'venue' => 'sometimes|nullable|string',
                    'notes' => 'sometimes|nullable|max:1048576',
                ]
            );
            $validateData['organisers'] = Auth::id();
            // $event = new Event();
            // $event->name = $validatedData['name'];
            // $event->description = $validatedData['description'];
            // $event->time = $validatedData['time'];
            // $event->venue = $validatedData['venue'];
            // $event->organisers = Auth::id();
            // $event->notes = $validatedData['notes'];
            // $event->save();
            $event = Event::create($validateData);

            return redirect()->route('events.show', $event)->with([
                'status' => "The topic has been successfully added",
                'type' => 'success'
            ]);
        } else {
            return abort(404, "Not Found");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        if(Auth::user()->id ==$event->organisers->id){
            
        } else {
            return abort(404, "Not Found");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        if(Auth::id() === $event->sponsor){
            $event->update(['deleted_at' => Carbon::now()]);
        }
    }
}
