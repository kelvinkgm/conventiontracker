<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use HasFactory, Notifiable, SoftDeletes;
    
    protected $fillable = [
        'name',
        'description',
        'time',
        'venue',
        'speakersSponsors','
        isCancelled',
        'organisers',
        'notes'
    ];

     /** Foreign Keys */

     public function sponsors(){
        return $this->belongsTo('App\User', 'speakersSponsors');
    }

    public function organiser(){
        return $this->belongsTo('App\User', 'organisers');
    }

    // public function prizes(){
    //     return $this->hasMany('App\Prize', 'relatedEvent');
    // }

    /** Model Based Queries */

    /**
     * Any Upcoming Events
     */
    public static function upcomingEvents(){
        return Event::where('time', '>', Carbon::now());
    }
    
    /**
     * Any Passed Events
     */
    public static function pastEvents(){
        return Event::where('time', '<', Carbon::now());
    }

    /**
     * Any random Events
     */
    public function randomEvents(){
        // return Event::upcomingEvents()
        return Event::where('time', '>', Carbon::now()->addMonth())->andWhere('time', '<', Carbon::now()->addYear())->inRandomOrder();
    }

    public static function validationRules(){
        return [
            'name' => 'required|string|max:200',
            'description' => 'required|string',
            'time' => 'sometimes|nullable|date',
            'venue' => 'sometimes|nullable|string',
            'notes' => 'sometimes|nullable|string|max:1048576',
        ];
    }

    public static function validationMessages(){
        return [
            'name.required' => 'This event needed a title.',
            'description.required' => 'A description is required for this event',
            'time.date' => 'The event needs to be in a date format',
        ];
    }
}
